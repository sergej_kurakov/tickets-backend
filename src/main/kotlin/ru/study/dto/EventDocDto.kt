package ru.study.dto

import java.math.BigDecimal

data class EventDocDto(
    var id: Long = 0,
    var guid: String = "",
    var image: String = "",
    var title: String = "",
    var description: String = "",
    var cost: BigDecimal = BigDecimal.ZERO
)