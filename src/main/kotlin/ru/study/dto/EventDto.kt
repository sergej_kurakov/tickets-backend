package ru.study.dto

import java.math.BigDecimal
import java.math.BigDecimal.ZERO
import java.util.*

data class EventDto(
    var id: Long = 0,
    var guid: String = "",
    var image: String = "",
    var title: String = "",
    var description: String = "",
    var cost: BigDecimal = ZERO
)
