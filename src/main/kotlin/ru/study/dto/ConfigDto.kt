package ru.study.dto

data class ConfigDto(
    var id: Long = 0,
    var clientID: String = "",
    var clientSecret: String = "",
    var notifyUrl: String? = ""
)