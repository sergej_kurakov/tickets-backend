package ru.study.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import ru.study.dto.EventDto
import ru.study.mapper.EventMapper
import ru.study.model.Purchase
import ru.study.repo.EventRepo
import ru.study.repo.PurchaseRepo

@Service
@Transactional
open class EventService @Autowired internal constructor(
    private val repo: EventRepo,
    private val purchaseRepo: PurchaseRepo,
    private val mapper: EventMapper
) {

    open fun findById(id: Long): EventDto {
        return mapper.map(repo.findById(id).orElse(null))
    }

    open fun purchase(id: Long, purchaseUuid: String) {
        purchaseRepo.deleteByUuid(purchaseUuid)
        val purchase = Purchase()
        purchase.uuid = purchaseUuid
        repo.findById(id).ifPresent { it.purchases.add(purchase) }
    }
}