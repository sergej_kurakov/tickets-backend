package ru.study.service

import com.paypal.base.rest.APIContext
import org.springframework.stereotype.Service
import ru.study.dto.ConfigDto

@Service
open class PayPalConfigService {

    fun createPayPalContext(configDto: ConfigDto): APIContext {
        return APIContext(configDto.clientID, configDto.clientSecret, "sandbox")
    }
}