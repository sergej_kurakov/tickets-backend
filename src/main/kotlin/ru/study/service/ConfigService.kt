package ru.study.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.study.dto.ConfigDto
import ru.study.mapper.ConfigMapper
import ru.study.repo.ConfigRepo

@Service
class ConfigService @Autowired internal constructor(
        private val repo: ConfigRepo,
        private val mapper: ConfigMapper
) {

    fun getConfig(): ConfigDto {
        return mapper.map(repo.findAll().single())
    }
}