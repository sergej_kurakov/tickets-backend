package ru.study.service

import com.paypal.api.payments.*
import com.paypal.base.rest.PayPalRESTException
import com.paypal.api.payments.Payment
import com.paypal.api.payments.PaymentExecution
import com.paypal.api.payments.RedirectUrls
import com.paypal.api.payments.Payer
import java.math.BigDecimal
import com.paypal.api.payments.Amount
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.study.enums.PaypalPaymentIntent
import ru.study.enums.PaypalPaymentMethod
import java.math.RoundingMode
import java.text.NumberFormat
import java.util.*

@Service
class PayPalService @Autowired internal constructor(
        private val configService: ConfigService,
        private val payPalConfigService: PayPalConfigService) {

    @Throws(PayPalRESTException::class)
    fun createPayment(
        total: BigDecimal?,
        currency: String,
        method: PaypalPaymentMethod,
        intent: PaypalPaymentIntent,
        description: String,
        cancelUrl: String,
        successUrl: String,
        notifyUrl: String
    ): Payment? {
        val config = configService.getConfig()

        var total = total
        val amount = Amount()
        amount.currency = currency
        total = total!!.setScale(2, RoundingMode.HALF_UP)
        amount.total = NumberFormat.getNumberInstance(Locale.GERMAN).format(total)

        val transaction = Transaction()
        transaction.description = description
        transaction.amount = amount
        transaction.notifyUrl = notifyUrl

        val transactions = ArrayList<Transaction>()
        transactions.add(transaction)

        val payer = Payer()
        payer.paymentMethod = method.toString()

        val payment = Payment()
        payment.intent = intent.toString()
        payment.payer = payer
        payment.transactions = transactions
        val redirectUrls = RedirectUrls()
        redirectUrls.cancelUrl = cancelUrl
        redirectUrls.returnUrl = successUrl
        payment.redirectUrls = redirectUrls

        return payment.create(payPalConfigService.createPayPalContext(config))
    }

    @Throws(PayPalRESTException::class)
    fun executePayment(paymentId: String, payerId: String): Payment? {
        val config = configService.getConfig()

        val payment = Payment()
        payment.id = paymentId
        val paymentExecute = PaymentExecution()
        paymentExecute.payerId = payerId

        return payment.execute(payPalConfigService.createPayPalContext(config), paymentExecute)
    }
}