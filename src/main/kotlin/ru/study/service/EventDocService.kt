package ru.study.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.study.dto.EventDocDto
import ru.study.mapper.EventDocMapper
import ru.study.repo.EventDocRepo

@Service
class EventDocService @Autowired internal constructor(
    private val repo: EventDocRepo,
    private val mapper: EventDocMapper
) {

    fun search(text: String): List<EventDocDto> {
		return if (text.isBlank())
			mapper.map(repo.findAll())
		else
			mapper.map(repo.search(text))
    }
}