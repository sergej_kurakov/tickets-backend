package ru.study.controller

import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.RestController
import java.nio.file.Files
import java.nio.file.Paths
import javax.servlet.http.HttpServletRequest

@RestController
class FilesController {

    private val STORAGE_URL = "/home/zeus1994/storage/"

    @RequestMapping(value = ["/api/file/{id}"])
    @ResponseBody
    fun getImage(@PathVariable id: Long): ByteArray {
        return Files.readAllBytes(Paths.get(STORAGE_URL + id))
    }
}