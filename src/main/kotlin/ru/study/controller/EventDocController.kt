package ru.study.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import ru.study.dto.EventDocDto
import ru.study.service.EventDocService

@RestController
class EventDocController @Autowired internal constructor(private val eventDocService: EventDocService) {

    @GetMapping("/api/events")
    fun search(text: String) : List<EventDocDto> {
        return try {
            eventDocService.search(text)
        } catch (throwable: Throwable) {
            println(throwable)
            listOf()
        }
    }
}