package ru.study.controller

import ru.study.service.PayPalService
import org.springframework.beans.factory.annotation.Autowired
import javax.servlet.http.HttpServletRequest
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import ru.study.enums.PaypalPaymentIntent
import ru.study.enums.PaypalPaymentMethod
import ru.study.service.ConfigService
import ru.study.service.EventService
import java.util.*

@RestController
class PaymentController @Autowired internal constructor(
        private val configService: ConfigService,
        private val eventService: EventService,
        private val payPalService: PayPalService
) {

    companion object {
        private const val SUCCESS_URL = "/api/pay/success"
        private const val NOTIFY_URL = "/api/pay/notify"
    }

    @RequestMapping(method = [ RequestMethod.POST ], value = [ "/api/pay" ])
    @ResponseStatus(HttpStatus.OK)
    fun pay(@RequestParam id: Long, request: HttpServletRequest): Map<String, Any> {
        val response = HashMap<String, Any>()
        val baseUrl = "${request.scheme}://${request.serverName}:${request.serverPort}"
        val payment = payPalService.createPayment(
            eventService.findById(id).cost,
            "RUB",
            PaypalPaymentMethod.paypal,
            PaypalPaymentIntent.sale,
            "Покупка билета",
            baseUrl,
            baseUrl,
            configService.getConfig().notifyUrl + NOTIFY_URL
                + "?id=" + id
                + "&purchaseUuid=" + UUID.randomUUID()
        )
        if (payment != null) {
            response["status"] = "success"
            response["id"] = payment.id
        }
        return response
    }

    @RequestMapping(method = [ RequestMethod.POST ], value = [ SUCCESS_URL ])
    @ResponseStatus(HttpStatus.OK)
    fun successPay(@RequestParam paymentID: String, @RequestParam payerID: String): Map<String, Any> {
        val response = HashMap<String, Any>()
        val payment = payPalService.executePayment(paymentID, payerID)
        if (payment != null) {
            response["status"] = "success"
        }
        return response
    }

    @RequestMapping(method = [ RequestMethod.POST ], value = [ NOTIFY_URL ])
    @ResponseStatus(HttpStatus.OK)
    fun notifyPay(@RequestParam id: Long, @RequestParam purchaseUuid: String) {
        eventService.purchase(id, purchaseUuid)
    }
}