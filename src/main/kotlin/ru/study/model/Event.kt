package ru.study.model

import java.math.BigDecimal
import java.math.BigDecimal.ZERO
import javax.persistence.*
import javax.persistence.CascadeType.*

@Entity
data class Event(

    @Id
    @GeneratedValue
    var id: Long = 0,

    var guid: String = "",
    var cost: BigDecimal = ZERO,

    @OneToMany(cascade = [ PERSIST, MERGE ])
    var purchases: MutableList<Purchase> = ArrayList()
)
