package ru.study.model

import org.springframework.data.elasticsearch.annotations.Document
import java.math.BigDecimal
import java.math.BigDecimal.ZERO
import javax.persistence.Id

@Document(indexName = "tickets")
data class EventDoc(

    @Id
    var id: Long = 0,

    var guid: String = "",
    var image: String = "",
    var title: String = "",
    var description: String = "",
    var cost: BigDecimal = ZERO
)