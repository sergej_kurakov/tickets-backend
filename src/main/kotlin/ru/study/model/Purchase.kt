package ru.study.model

import java.time.LocalDateTime
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
data class Purchase(

    @Id
    @GeneratedValue
    var id: Long = 0,

    var uuid: String = "",
    var date: LocalDateTime = LocalDateTime.now()
)