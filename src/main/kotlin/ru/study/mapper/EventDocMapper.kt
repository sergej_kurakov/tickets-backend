package ru.study.mapper

import org.mapstruct.Mapper
import ru.study.dto.EventDocDto
import ru.study.model.EventDoc

@Mapper(componentModel = "spring")
interface EventDocMapper {
    fun map(source: Iterable<EventDoc>): List<EventDocDto>
}