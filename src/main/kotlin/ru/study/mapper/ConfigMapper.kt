package ru.study.mapper

import org.mapstruct.Mapper
import ru.study.dto.ConfigDto
import ru.study.model.Config

@Mapper(componentModel = "spring")
interface ConfigMapper {
    fun map(source: Config): ConfigDto
}