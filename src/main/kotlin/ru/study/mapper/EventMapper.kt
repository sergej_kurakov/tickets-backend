package ru.study.mapper

import org.mapstruct.Mapper
import ru.study.dto.EventDto
import ru.study.model.Event

@Mapper(componentModel = "spring")
interface EventMapper {
    fun map(source: Event): EventDto
}
