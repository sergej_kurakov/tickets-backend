package ru.study.repo

import org.springframework.data.repository.CrudRepository
import ru.study.model.Config

interface ConfigRepo : CrudRepository<Config, Long>