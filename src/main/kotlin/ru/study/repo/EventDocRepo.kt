package ru.study.repo

import org.springframework.data.elasticsearch.annotations.Query
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository
import ru.study.model.EventDoc

interface EventDocRepo : ElasticsearchRepository<EventDoc, Long> {

    @Query("{\"bool\": {\"must\": [{\"match\": {\"title\": \"?0\"}}]}}")
    fun search(title: String): List<EventDoc>
}