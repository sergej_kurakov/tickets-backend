package ru.study.repo

import org.springframework.data.repository.Repository
import ru.study.model.Purchase

interface PurchaseRepo : Repository<Purchase, Long> {
    fun deleteByUuid(uuid: String)
}