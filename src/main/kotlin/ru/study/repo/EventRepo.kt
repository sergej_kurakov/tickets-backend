package ru.study.repo

import org.springframework.data.repository.CrudRepository
import ru.study.model.Event

interface EventRepo : CrudRepository<Event, Long>