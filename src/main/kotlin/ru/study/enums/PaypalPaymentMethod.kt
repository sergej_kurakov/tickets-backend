package ru.study.enums

enum class PaypalPaymentMethod {
    credit_card, paypal
}