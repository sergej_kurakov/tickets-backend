package ru.study.enums

enum class PaypalPaymentIntent {
    sale, authorize, order
}