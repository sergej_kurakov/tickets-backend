package ru.study.config

import org.elasticsearch.client.Client
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate
import org.springframework.data.elasticsearch.core.ElasticsearchOperations
import org.springframework.context.annotation.Bean
import java.net.InetAddress
import org.elasticsearch.transport.client.PreBuiltTransportClient
import org.elasticsearch.common.settings.Settings
import org.elasticsearch.common.transport.TransportAddress
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories

@Configuration
@EnableElasticsearchRepositories(basePackages = [ "ru.study.repo" ])
open class ElasticConfig {

    @Value("\${elasticsearch.home:/usr/share/elasticsearch}")
    private val elasticsearchHome: String? = null

    @Value("\${elasticsearch.cluster.name:elasticsearch}")
    private val clusterName: String? = null

    @Bean
    open fun client(): Client {
        val elasticsearchSettings = Settings.builder()
            .put("client.transport.sniff", true)
            .put("path.home", elasticsearchHome)
            .put("cluster.name", clusterName).build()
        val client = PreBuiltTransportClient(elasticsearchSettings)
        client.addTransportAddress(TransportAddress(InetAddress.getByName("127.0.0.1"), 9300))
        return client
    }

    @Bean
    open fun elasticsearchTemplate(): ElasticsearchTemplate {
        return ElasticsearchTemplate(client())
    }
}